

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Christian Burell - Information Technology Major

### Assignment 4 Requirements:


#### README.md file should include the following items:

1. Mortgage Calculator
2. Splash Screen
3. Invalid entry for years



#### Assignment Screenshot and Links:
First                                        |  Second 
:------------------------------------------:|:------------------------------------------:
![First](img/1.png)          |![Second](img/2.png)

Third                                        |  Fourth
:------------------------------------------:|:------------------------------------------:
![First](img/3.png)          |![Second](img/4.png)







