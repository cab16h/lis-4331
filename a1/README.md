
# Android Studio

## Christian Burell - Information Technology
## These projects show example Android Studio projects I have designed and developed. Feel free to go through the code in the A1 section above to review my work. 

###Screenshots:

One                                         |  Two
:------------------------------------------:|:------------------------------------------:
![1](img/3.png)                             |![2](img/4.png)


Third                                       |  Fourth
:------------------------------------------:|:------------------------------------------:
![3](img/5.png)                             |![4](img/6.png)


Five                                        |  Six 
:------------------------------------------:|:------------------------------------------:
![5](img/7.png)                             |![6](img/8.png)







