package com.example.tasklist;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class TaskList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceStance) {
        super.onCreate(savedInstanceStance);
        setContentView(R.layout.activity_task_list);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_trending_up_black_24dp);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        TaskListDB db = new TaskListDB(this);
        StringBuilder sb = new StringBuilder();

        Task task = new Task(1, "See an FSU National Championship", "" , "0", "0");
        long insertId= db.insertTask(task);
        if (insertId > 0) {
            sb.append("Row inserted! Insert Id: " + insertId + "\n");
        }

        Task task2= new Task(1, "Watch the NFL draft", "","0","0");
        long insertId2 = db.insertTask(task2);
        if (insertId2 > 0) {
            sb.append("Row inserted! Insert Id: " + insertId2 + "\n");
        }

        task.setId((int) insertId);
        task.setName("Update Test");
        int updateCount = db.updateTask(task);
        if (updateCount == 1) {
            sb.append("Task updated! Update count: " + updateCount + "\n");
        }

        int deleteCount = db.deleteTask(insertId);
        if (deleteCount == 1) {
            sb.append("Task deleted! Delete count: " + deleteCount + "\n\n");
        }

        db.deleteTask(5);
        db.deleteTask(7);

        ArrayList<Task> tasks=db.getTasks("Personal");
        for (Task t : tasks) {
            sb.append(t.getId() + " | " + t.getName() + "\n");
        }

        TextView taskListTextView = (TextView)
                findViewById(R.id.taskListTextView);
        taskListTextView.setText(sb.toString());

    }
}
