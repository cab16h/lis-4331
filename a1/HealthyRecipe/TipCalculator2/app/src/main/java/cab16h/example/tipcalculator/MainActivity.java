package cab16h.example.tipcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    double billCost;
    double totalguest;
    double tippercent;
    double individual;
    double total;
    String number;
    String percentage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.food);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText bill = (EditText)findViewById(R.id.txtResult);
        final Spinner guest = (Spinner)findViewById(R.id.txtGroup);
        final Spinner percent = (Spinner)findViewById(R.id.spinner);
        Button cost = (Button)findViewById(R.id.btnCost);
        cost.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v) {
                billCost = Double.parseDouble(bill.getText().toString());

                DecimalFormat currency = new DecimalFormat("$###,###.##");

                number = guest.getSelectedItem().toString();
                totalguest = Double.parseDouble(number);

                percentage = percent.getSelectedItem().toString();
                tippercent = Double.parseDouble(percentage);

                individual = (billCost/totalguest);

                total = ((individual) + ((individual) * (tippercent/100)));

                result.setText("Cost for each of " + number + " guest: " + currency.format(total));
            }
        });

    }

}}