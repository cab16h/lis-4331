package cab16h.example.my_music;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    Button button1, button2, button3;
    MediaPlayer mpLukeCombs, mpTobyKeith, mpKennyChesney;
    int playing;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        button1 = (Button) findViewById(R.id.btnLukeCombs);
        button2 = (Button) findViewById(R.id.btnTobyKeith);
        button3 = (Button) findViewById(R.id.btnKennyChesney);
        button1.setOnClickListener(bLuke);
        button2.setOnClickListener(bToby);
        button3.setOnClickListener(bKenny);
        mpLukeCombs = new MediaPlayer();
        mpLukeCombs = MediaPlayer.create(this, R.raw.luke);
        mpTobyKeith = new MediaPlayer();
        mpTobyKeith = MediaPlayer.create(this, R.raw.toby);
        mpKennyChesney = new MediaPlayer();
        mpKennyChesney = MediaPlayer.create(this, R.raw.kenny);
        playing = 0;
    }
    Button.OnClickListener bLuke = new Button.OnClickListener() {
        @Override
        public void onClick (View v) {
            switch(playing) {
                case 0:
                    mpLukeCombs.start();
                    playing=1;
                    button1.setText("Pause Luke Combs Song");
                    button2.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpLukeCombs.pause();
                    playing=0;
                    button1.setText("Play Luke Combs Song");
                    button2.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };
    Button.OnClickListener bToby = new Button.OnClickListener() {
        @Override
        public void onClick (View v) {
            switch(playing) {
                case 0:
                    mpTobyKeith.start();
                    playing=1;
                    button2.setText("Pause Toby Keith Song");
                    button1.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpTobyKeith.pause();
                    playing=0;
                    button2.setText("Play Toby Keith Song");
                    button1.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };
    Button.OnClickListener bKenny = new Button.OnClickListener() {
        @Override
        public void onClick (View v) {
            switch(playing) {
                case 0:
                    mpKennyChesney.start();
                    playing=1;
                    button3.setText("Pause Kenny Chesney Song");
                    button2.setVisibility(View.INVISIBLE);
                    button1.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpKennyChesney.pause();
                    playing=0;
                    button3.setText("Play Kenny Chesney Song");
                    button2.setVisibility(View.VISIBLE);
                    button1.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };
}

