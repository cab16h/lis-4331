package cab16h.example.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import java.text.DecimalFormat;


import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    double conversionRateEuros = 0.93;
    double conversionRatePesos = 18.59;
    double conversionRateCanadian = 1.3;
    double dEntered;
    double convertedD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText USD = (EditText) findViewById(R.id.txtUSD);
        final CheckBox euros = (CheckBox) findViewById(R.id.radEuros);
        final CheckBox pesos = (CheckBox) findViewById(R.id.radPesos);
        final CheckBox canadian = (CheckBox) findViewById(R.id.radCanadian);
        final TextView result = (TextView) findViewById(R.id.txtResult);
        Button convert = (Button) findViewById(R.id.btnConvert);

        convert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dEntered=Double.parseDouble(USD.getText().toString());
                DecimalFormat tenth = new DecimalFormat("#########.##");


                if(euros.isChecked()){
                    if(dEntered <=100000){

                        convertedD = dEntered * conversionRateEuros;

                        result.setText("€" + tenth.format(convertedD) + " Euros");
                    }else {
                        Toast.makeText(MainActivity.this, "Dollars must be lest than 100,000", Toast.LENGTH_LONG).show();
                    }
                }

                if(pesos.isChecked()){
                    if(dEntered <=100000){

                        convertedD = dEntered * conversionRatePesos;

                        result.setText("₱" +tenth.format(convertedD) + " Pesos");
                    }else {
                        Toast.makeText(MainActivity.this, "Dollars must be lest than 100,000", Toast.LENGTH_LONG).show();
                    }
                }

                if(canadian.isChecked()){
                    if(dEntered <=100000){

                        convertedD = dEntered * conversionRateCanadian;

                        result.setText("CA" + tenth.format(convertedD) + " Canadian Dollars");
                    }else {
                        Toast.makeText(MainActivity.this, "Dollars must be less than 100,000", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

}