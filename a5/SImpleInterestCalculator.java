import java.util.Scanner;
import java.text.DecimalFormat;

public class SimpleInterestCalculator
{
   public static void main(String args[])
   {
    double principal, rate, simpleInterest;
    int time;
    // String principalString;
    Scanner sc = new Scanner(System.in);
    System.out.print("Program performs the following functions: \n1. Calculates amount of money saved for a period of years, at a specified interest rate (i.e., yearly, non-compounded) \n2. Returned amount is formatted in U.S. currency, and rounded to two decimal places.");
    
    System.out.print("\n\nCurrent principal: $");
    while (!sc.hasNextDouble()) {
      System.out.print("Not a valid number!");
      System.out.print("\n\nPlease try again. Enter principal: ");
      sc.next();
    }
    principal = sc.nextDouble();

    System.out.print("\nInterest Rate (per year): ");
    while (!sc.hasNextDouble()) {
      System.out.print("Not a valid number!");
      System.out.print("\n\nPlease try again. Enter interest rate: ");
      sc.next();
    }
    rate = sc.nextDouble();

    System.out.print("\nTotal time (in years): ");
    while (!sc.hasNextInt()) {
      System.out.print("Not a valid integer!");
      System.out.print("\n\nPlease try again. Enter years: ");
      sc.next();
    }
    time = sc.nextInt();

    simpleInterest = (principal * (1 + time * rate)) / 100;
    DecimalFormat dFormat = new DecimalFormat("$####,###,###.##");
    // System.out.println("$" + dFormat.format(amt));
    System.out.println("\nYou will have saved " + dFormat.format(simpleInterest) + " in " + time + " years, " + "at an interest rate of " + rate + "%");

    sc.close();
    }
  }