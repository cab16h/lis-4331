

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Christian Burell - Information Technology Major

### Project 2 Requirements:


#### README.md file should include the following items:

1. Screenshot of tasks
2. Screenshot of splash screen
3. At least 5 tasks


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshot and Links:
Splash Screen                    |  Tasks
:------------------------------------------:|:------------------------------------------:
![Splash](img/1.png)     |![Tasks](img/2.png)





