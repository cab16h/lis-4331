
# My Work
## Christian Burell - Information Technology
## Florida State University

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Android Studio Projects
    - Java programming 

2. [A2 README.md](a2/README.md "My A1 README.md file")
    - Python programs
    - Examples of my Python work

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - HTML and CSS examples
    - My example site with various functions
    

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - MYSQL work 
    - Database projects
    
